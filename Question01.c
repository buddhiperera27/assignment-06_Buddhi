#include <stdio.h>

void pattern(int num ) 
{ 
  int i , x = 1;
  while (num >= x) 
  {
    for ( i = x; i > 0 ; i-- )
    {
      printf( "%d" , i );
    }
    printf("\n");
    x++;

  }
}

int main(void)
{
  int num;

  printf( "Enter a positive number : " );
  scanf( "%d" , &num );

  pattern(num);

  return 0;
}