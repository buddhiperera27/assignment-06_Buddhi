#include <stdio.h>

int fibonacciSeq(int fiboNumber);

int main(void) {
    int fiboNumber, fibonacciSequence;
    printf("Enter the number of terms needs to print Fibonacci sequence: ");
    scanf("%d", &fiboNumber);
    printf("Fibonacci Sequence: \n");

    fibonacciSeq(fiboNumber);

    return 0;
}

int fibonacciSeq(fiboNumber){
    int fOne = 0, fTwo = 1, nextFTerm;
    for (int i = 0; i <= fiboNumber; ++i) {
        printf("%d \n", fOne);
        nextFTerm = fOne + fTwo;
        fOne = fTwo;
        fTwo = nextFTerm;
    }
}